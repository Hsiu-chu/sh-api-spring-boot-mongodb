package com.sh.api.mongo.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.mapreduce.GroupBy;
import org.springframework.data.mongodb.core.mapreduce.GroupByResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
  * 类名：MongoGenDao.java
  * 类说明： mongoDb数据访问抽象类
  * Copyright: Copyright (c) 2012-2020
  * Company: HT
  * @author     haoxz
  * @date       2020年4月2日
  * @version    1.0
*/
public abstract class MongoGenDao<T> {
    private final static Logger LOGGER = LoggerFactory.getLogger(MongoGenDao.class);

    @Autowired  
    MongoTemplate mongoTemplate;

    /**
      * 方法：save 
      * 方法说明：    添加对象到数据库
      * @param t
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void save(T t) {
        this.mongoTemplate.save(t);
    }
    /**
      * 方法：batchInsert 
      * 方法说明：    批量插入数据
      * @param list
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void batchInsert(List<T> list) {
        this.mongoTemplate.insertAll(list);
    }
    
    /**
      * 方法：queryById 
      * 方法说明：    根据Id从库中查询对象
      * @param id
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public T queryById(String id) {
        Query query = new Query();
        Criteria criteria = Criteria.where("_id").is(id);
        query.addCriteria(criteria);
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]queryById:" + query);
        }
        return this.mongoTemplate.findOne(query, this.getEntityClass());
    }

    /**
      * 方法：queryList 
      * 方法说明：    根据条件查询集合
      * @param query
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public List<T> queryList(Query query) {
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]queryList:" + query);
        }
        return this.mongoTemplate.find(query, this.getEntityClass());
    }

    /**
      * 方法：queryOne 
      * 方法说明：    根据条件查询单一对象
      * @param query
      * @return
      * @author     haoxz
      * @date      2020年4月2日
     */
    public T queryOne(Query query) {
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]queryOne:" + query);
         }
        return this.mongoTemplate.findOne(query, this.getEntityClass());
    }
    /**
      * 方法：getPageCount 
      * 方法说明：    根据条件查询库中符合记录的总数,为分页查询服务
      * @param query
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public Long getPageCount(Query query){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]queryPageCount:" + query);
        }
        return this.mongoTemplate.count(query, this.getEntityClass());
    }
    /**
      * 方法：getPage 
      * 方法说明：    通过条件进行分页查询
      * @param query
      * @param start
      * @param size
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public List<T> getPage(Query query, int start, int size) {
        query.skip(start);
        query.limit(size);
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]queryPage:" + query + "(" + start + "," + size+ ")");
        }
        List<T> lists = this.mongoTemplate.find(query, this.getEntityClass());
        return lists;
    }
    /**
      * 方法：group 
      * 方法说明：    分组查询
      * @param inputCollectionName
      * @param groupBy
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public GroupByResults<T> groupData(String inputCollectionName,GroupBy groupBy){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]group:" + groupBy + "(" + inputCollectionName + ")");
        }
        return mongoTemplate.group(inputCollectionName, groupBy, this.getEntityClass()); 
    }
    /**
      * 方法：aggregate 
      * 方法说明：    聚合
      * @param inputCollectionName
      * @param agg
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public AggregationResults<T> aggregate(String inputCollectionName,Aggregation agg){
        if(LOGGER.isInfoEnabled()){
            LOGGER.info("[Mongo Dao ]group:" + agg + "(" + inputCollectionName + ")");
        }
        return mongoTemplate.aggregate(agg,inputCollectionName, this.getEntityClass()); 
    }
    /**
     * 方法：group 
     * 方法说明：    分组查询
     * @param inputCollectionName
     * @param groupBy
     * @return
     * @author     haoxz
     * @date       2020年4月2日
    */
   public GroupByResults<T> groupData(Criteria criteria,String inputCollectionName,GroupBy groupBy){
       if(LOGGER.isDebugEnabled()){
           LOGGER.debug("[Mongo Dao ]group:" + groupBy + "(" + inputCollectionName + ")");
       }
       return mongoTemplate.group(criteria,inputCollectionName, groupBy, this.getEntityClass()); 
   }
    /**
      * 方法：deleteById 
      * 方法说明：    根据Id删除用户
      * @param id
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void deleteById(String id) {
        Criteria criteria = Criteria.where("_id").in(id);
        if(null!=criteria){
            Query query = new Query(criteria);
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("[Mongo Dao ]deleteById:" + query);
            }
            if(null!=query && this.queryOne(query)!=null){
                this.delete(queryOne(query));
            }
        }
    }
    /**
      * 方法：delete 
      * 方法说明：    删除对象
      * @param t
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void delete(T t){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]delete:" + t);
        }
        this.mongoTemplate.remove(t);
    }
    /**
     *方法:delete
     *方法说明: 按照查询条件以及表名删除数据
     *@author     haoxiuzhu
     *@date       2020/8/28
     *@Param      * @param query: 
     * @param collectionName: 
     *@retrun     * @return: void
     */
    public void delete(Query query,String collectionName){
        if(LOGGER.isInfoEnabled()){
            LOGGER.info("[Mongo Dao ]delete:" + query+",collectionName:"+collectionName);
        }
        this.mongoTemplate.remove(query,collectionName);
    }
    /**
     *方法:delete
     *方法说明: 按照查询条件删除数据
     *@author     haoxiuzhu
     *@date       2020/8/28
     *@Param      * @param query: 
     *@retrun     * @return: void
     */
    public void delete(Query query){
        if(LOGGER.isInfoEnabled()){
            LOGGER.info("[Mongo Dao ]delete:" + query);
        }
        this.mongoTemplate.remove(query,this.getEntityClass());
    }
    /**
      * 方法：updateFirst 
      * 方法说明：    更新满足条件的第一个记录
      * @param query
      * @param update
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void updateFirst(Query query,Update update){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]updateFirst:query(" + query + "),update(" + update + ")");
        }
        this.mongoTemplate.updateFirst(query, update, this.getEntityClass());
    }
    /**
      * 方法：updateMulti 
      * 方法说明：    更新满足条件的所有记录
      * @param query
      * @param update
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void updateMulti(Query query, Update update){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]updateMulti:query(" + query + "),update(" + update + ")");
        }
        this.mongoTemplate.updateMulti(query, update, this.getEntityClass());
    }
    /**
      * 方法：updateInser 
      * 方法说明：    查找更新,如果没有找到符合的记录,则将更新的记录插入库中
      * @param query
      * @param update
      * @author     haoxz
      * @date       2020年4月2日
     */
    public void updateInser(Query query, Update update){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]updateInser:query(" + query + "),update(" + update + ")");
        }
        this.mongoTemplate.upsert(query, update, this.getEntityClass());
    }
    /**
      * 方法：getCount 
      * 方法说明：    查询数量
      * @param query
      * @param collectionName
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    public Long getCount(Query query,String collectionName){
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("[Mongo Dao ]getCount:" + query+",collectionName:"+collectionName);
        }
        return this.mongoTemplate.count(query,collectionName);
    }
    /***
      * 方法：getEntityClass 
      * 方法说明：    由子类实现返回反射对象的类型
      * @return
      * @author     haoxz
      * @date       2020年4月2日
     */
    protected abstract Class<T> getEntityClass();
}
